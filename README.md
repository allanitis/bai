# hello-world

Simplest, fastest way to get a simple webapp up

Uses small nodeJS server to answer "hi world",
leveraging the blazing-fast alpine-OS backed [node] docker container.

